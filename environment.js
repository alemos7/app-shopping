import Constants from "expo-constants";

const PRODUCTION = 'production';
const STAGING = 'staging';

const ENV = {
    dev: {
        iosClientId: '963047768979-a0kbrsv8vvp0k0s570m79c16851sfn7s.apps.googleusercontent.com',
        androidClientId: '963047768979-ud5hu89a1vood9k4cnne02g76ona5j1n.apps.googleusercontent.com',
        iosStandaloneAppClientId: '963047768979-a0kbrsv8vvp0k0s570m79c16851sfn7s.apps.googleusercontent.com',
        androidStandaloneAppClientId: '963047768979-ud5hu89a1vood9k4cnne02g76ona5j1n.apps.googleusercontent.com'
    },
    staging: {
        iosClientId: null,
        androidClientId: null,
        iosStandaloneAppClientId: null,
        androidStandaloneAppClientId: null
    },
    production: {
        iosClientId: null,
        androidClientId: null,
        iosStandaloneAppClientId: null,
        androidStandaloneAppClientId: null
    }
};

export default (env = Constants.manifest.releaseChannel) => {
    if (__DEV__) {
        return ENV.dev;
    }else if (env === STAGING) {
        return ENV.staging;
    }else if (env === PRODUCTION) {
        return ENV.production;
    }
}