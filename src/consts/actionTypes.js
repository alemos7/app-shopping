export const GET_LOCATIONS_START = '@udemy:GET_LOCATIONS_START';
export const GET_LOCATIONS_SUCCESS = '@udemy:GET_LOCATION_SUCCESS';
export const GET_LOCATIONS_ERROR = '@udemy:GET_LOCATIONS_ERROR';

export const GET_ITINERARIES_START = '@udemy:GET_ITINERARIES_START';
export const GET_ITINERARIES_SUCCESS = '@udemy:GET_ITINERARIES_SUCCESS';
export const GET_ITINERARIES_ERROR = '@udemy:GET_ITINERARIES_ERROR';

export const GET_LOGIN_START = '@udemy:GET_LOGIN_START';
export const GET_LOGIN_SUCCESS = '@udemy:GET_LOGIN_SUCCESS';
export const GET_LOGIN_ERROR = '@udemy:GET_LOGIN_ERROR';
