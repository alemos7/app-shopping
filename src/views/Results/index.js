import React, { useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import { View, Text } from 'react-native';


import { getItineraries } from "../../redux/actions/itineraries";

export default function Result({ navigation }) {

    const dispatch = useDispatch();
    const [hasFetched, setHasFetched] = useState(false);

    // const {
    //     state: {
    //         params: {
    //             adults,
    //             children,
    //             originPlace,
    //             destinationPlace,
    //             inboundDate,
    //             outboundDate
    //         }
    //     }
    // } = navigation;

    useEffect( () => {
        if (!hasFetched) {
           dispatch(getItineraries({
               adults: 1,
               children: 0,
               originPlace: "SFO-sky",
               destinationPlace: "LHR-sky",
               inboundDate: '2020-01-01',
               outboundDate: '2019-12-24'
           }));
           setHasFetched(true);
        }
    });

    return (
        <View>
            <Text>
                Result
            </Text>
        </View>
    );
}
