import React, { useState, useEffect } from 'react';
import { Image } from 'react-native';
import * as Google from 'expo-google-app-auth';
import * as Facebook from 'expo-facebook';
import { useDispatch, connect, useSelector } from "react-redux";

import {Container, Icon, Content, Text, Input, Grid, Button, Item, Form, ListItem, List} from "native-base";

import styles from './style';
import genericStyles from "../../styles";
import environment from "../../../environment";
import { saveItem } from "../../utils/storage";
import { ACCESS_TOKEN, USER_INFO, GOOGLE_SUCCESS_MESSAGE, HOME } from "../../consts";
import {Alert} from "react-native-web";
import {getLocations} from "../../redux/actions/itineraries";
import {getToken} from "../../redux/actions/oauth";
import FixedList from "../../components/FixedList";

const GOOGLE_IMAGE = require('../../../assets/google.png');
const FACEBOOK_IMAGE = require('../../../assets/facebook.png');
const {
    iosClientId,
    androidClientId,
    androidStandaloneAppClientId,
    iosStandaloneAppClientId
} = environment();

export default function Login( { navigation }) {

    const [username, setUsername] = useState();
    const [password, setPassword] = useState();

    const handleUsernameChange = text => setUsername(text);
    const handlePasswordChange = text => setPassword(text);

    const dispatch = useDispatch();
    const oauthToken = useSelector(state => state.itineraries.token);

    const handleOauthLoginPress = () => {
        try {
           dispatch( getToken({ username: username, password: password}));
        } catch (e) {
            alert('Eror al ingresar oauth: '+ e);
        }
    };


    const handleGoogleLoginPress = async () => {
      try {
          const { user, accessToken, type } = await Google.logInAsync({
              iosClientId,
              androidClientId,
              androidStandaloneAppClientId,
              iosStandaloneAppClientId
          });

          if (type === GOOGLE_SUCCESS_MESSAGE){
              const userResult = await saveItem(USER_INFO, JSON.stringify(user));
              const tokenResult = await saveItem(ACCESS_TOKEN, accessToken);
              // console.log(user, userResult, tokenResult);
              if (userResult && tokenResult) {
                navigation.navigate(HOME);
              }else{
                  alert("Error al iniciar sesion");
              }
          }
      } catch (e) {
          alert('Eror: '+ e);
      }
    };

    const handleFacebookLoginPress = async function logIn() {
        try {
            await Facebook.initializeAsync('616778212574438');
            const {
                type,
                token,
                expires,
                permissions,
                declinedPermissions,
            } = await Facebook.logInWithReadPermissionsAsync({
                permissions: ['public_profile', 'email'],
            });
            if (type === 'success') {
                // const response = await fetch(`https://graph.facebook.com/me?access_token=${token}&fields=id,name,email,about,picture`);
                const response = await fetch(`https://graph.facebook.com/me?access_token=${token}&fields=name,email`);
                // console.log(JSON.stringify(permissions));
                // console.log("URL FACEBOOK: "+`https://graph.facebook.com/me?access_token=${token}&fields=name,email`);
                const responseJSON = await response.json();
                // console.log("FACEBOOK: "+JSON.stringify(responseJSON));
                const idUser = responseJSON.id;
                if (idUser) {
                    navigation.navigate(HOME);
                }else{
                    alert("Error al iniciar sesion");
                }
            } else {
                // type === 'cancel'
                alert("Error al iniciar sesion");
            }
        } catch ({ message }) {
            alert(`Facebook Login Error: ${message}`);
        }
    }

    return (
        <Container>
            <Content contentContainerStyle={[genericStyles.centeredContent, styles.content]}>
                <Text style={styles.title}>¡Bienvenidos a Shopping!</Text>
                <Text style={styles.subtitle}>Inicia sesion para continuar</Text>

                <Form>
                    <Item>
                        <Icon name="ios-airplane"/>
                        <Input
                            onChangeText={handleUsernameChange}
                            placeholder="Username" />
                    </Item>
                    <Item last>
                        <Icon name="ios-airplane"/>
                        <Input
                            onChangeText={handlePasswordChange}
                            placeholder="Password" />
                    </Item>
                    <Button onPress={handleOauthLoginPress} style={styles.googleBtn} primary><Text> Primary </Text></Button>
                </Form>


                <Grid style={[genericStyles.centeredGrid, styles.grid]}>
                    <Button light style={styles.socialBtn} onPress={handleGoogleLoginPress}>
                        <Image source={GOOGLE_IMAGE} style={styles.googleIcon} />
                    </Button>
                    <Button light style={styles.socialBtn} onPress={handleFacebookLoginPress}>
                        <Image source={FACEBOOK_IMAGE} style={styles.facebookIcon} />
                    </Button>
                </Grid>



            </Content>
        </Container>
    );
};

// <string name="facebook_app_id">616778212574438</string> <string name="fb_login_protocol_scheme">fb616778212574438</string>
