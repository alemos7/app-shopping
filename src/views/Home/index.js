import React, { useEffect, useState } from 'react';
import { Container, Content, Text, Header, Grid } from 'native-base';

import MyHeader from '../../components/Header';
import SearchComponent from "../../components/SearchComponent";
import {USER_INFO} from "../../consts";
import {getItem} from "../../utils/storage";
import styles from "./style";
import genericStyle from "../../styles"

export default function Home({navigation}) {
    const [userInfo, setUserInfo] = useState(null);
    useEffect(
        () => {
            if (!userInfo) {
                loadUserInfo();
            }
        },
        [userInfo]
        );

    const loadUserInfo = async  () => {
        let userInfo = await getItem(USER_INFO);
        userInfo = JSON.parse(userInfo);
        console.log(userInfo);
        setUserInfo(userInfo);
    };

    return (
        <Container>
            <MyHeader imageUri={userInfo && userInfo.photoUrl}/>
            <Content contentContainerStyle={genericStyle.centeredContent}>
                <Grid style={genericStyle.centeredGrid}>
                    <SearchComponent navigation={navigation} />
                </Grid>
            </Content>
        </Container>
    );
}