import React from 'react';
import { Header, Left, Body, Right, Thumbnail} from "native-base";

import {PRIMARY, PRIMARY_DARK} from "../../consts";
import styles from './style';

export default function  MyHeader({imageUri}) {
    return (
        <Header androidStatusBarColor={PRIMARY_DARK} style={styles.header}>
            <Left/>
            <Body/>
            <Right>
                <Thumbnail source={{ uri: imageUri && imageUri}} small/>
            </Right>
        </Header>
    );
}