import { all } from 'redux-saga/effects';

import itineraries from "./iteneraries";
import oauth from "./oauth";

export default function* rootSaga() {
    yield all([
        itineraries(),
        oauth()
    ])
}