import {
    GET_LOGIN_START,
    GET_LOGIN_ERROR,
    GET_LOGIN_SUCCESS
} from "../../consts/actionTypes";

import {
    takeLatest,
    call,
    put
} from 'redux-saga/effects';

import apiCallOauth from '../api/oauth';

export function* getToken({payload: {
    username,
    password
}   }) {
    try {
        var body = new URLSearchParams();
        body.append("username", username);
        body.append("password", password);
        body.append("grant_type", "password");

        const results = yield call(
            apiCallOauth,
            'api/security/oauth/token',
            'POST',
            body,
            // headers
        );
        yield put({type: GET_LOGIN_SUCCESS , results: results});
    }catch (error) {
        yield put({type: GET_LOGIN_ERROR, error});
    }

}


export default function* oauth() {
    yield takeLatest(GET_LOGIN_START, getToken);
}