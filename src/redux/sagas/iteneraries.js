import {
    GET_LOCATIONS_START,
    GET_LOCATIONS_ERROR,
    GET_LOCATIONS_SUCCESS,
    GET_ITINERARIES_START,
    GET_ITINERARIES_SUCCESS,
    GET_ITINERARIES_ERROR, GET_LOGIN_START
} from "../../consts/actionTypes";

import {
    takeLatest,
    call,
    put
} from 'redux-saga/effects';

import apiCall from '../api';
import apiCallOauth from '../api/oauth';

const country = 'MX';
const currency = 'USD';
const locale = 'en-US';

export function* getLocations({payload}) {
    try {
        const url = `/autosuggest/v1.0/${country}/${currency}/${locale}/?query=${payload.query}`;
        const method = 'GET';
        const results = yield call(apiCall, url, method);
        // console.log(results.data);
        yield put({type: GET_LOCATIONS_SUCCESS , results: results.data.Places});
    }catch (error) {
       yield put({type: GET_LOCATIONS_ERROR, error});
    }
}

export function* getItineraries({payload: {
    adults,
    children,
    originPlace,
    destinationPlace,
    inboundDate,
    outboundDate
}   }) {
    try {

        const body = {
            adults,
            children,
            originPlace,
            destinationPlace,
            inboundDate,
            outboundDate,
            country,
            currency,
            locale
        };
        const headers = {
            "content-type": "application/x-www-form-urlencoded"
        };
        const sessionKeyResult = yield call(
            apiCall,
            'pricing/v1.0',
            'POST',
            new URLSearchParams(body),
            headers
        );

        console.log(sessionKeyResult, 10);

    }catch (error) {
        // yield put({type: GET_ITINERARIES_ERROR, error});
        console.log(error, 20);

    }

}

export default function* itineraries() {
    yield takeLatest(GET_LOCATIONS_START, getLocations);
    yield takeLatest(GET_ITINERARIES_START, getItineraries);
}