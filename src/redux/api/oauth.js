import axios from 'axios';

const baseURL = 'http://192.168.0.120:8090';

export default (url, method, data, headers) =>
    axios({
        baseURL,
        method,
        url,
        data,
        headers: {
            'Authorization': 'Basic ZnJvbnRlbmRhcHA6MTIzNDU=',
            'Content-Type': 'application/x-www-form-urlencoded',
            ...headers
        }
    });


