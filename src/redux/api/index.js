import axios from 'axios';

const baseURL = 'https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices';

export default (url, method, data, headers) =>
    axios({
        baseURL,
        method,
        url,
        data,
        headers: {
                "x-rapidapi-host": "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com",
                "x-rapidapi-key": "213e3f77demsh82617d43ce11a4cp16aa90jsn21b3e657ec10",
                ...headers
        }
    });
