import { combineReducers } from 'redux';

import itineraries from './itineraries';
// import oauth from './itineraries';

export default combineReducers({
    itineraries
});