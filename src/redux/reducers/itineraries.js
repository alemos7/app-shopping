import {
   GET_ITINERARIES_ERROR,
   GET_ITINERARIES_START,
   GET_ITINERARIES_SUCCESS,
   //
   GET_LOCATIONS_ERROR,
   GET_LOCATIONS_START,
   GET_LOCATIONS_SUCCESS,
   //
   GET_LOGIN_ERROR,
   GET_LOGIN_START,
   GET_LOGIN_SUCCESS
} from "../../consts/actionTypes";

export default function(state, action) {
   switch (action.type) {
      case GET_LOCATIONS_START:
          return  { ...state};
          break;
      case GET_LOCATIONS_SUCCESS:
         return {...state, places: action.results};
         break;
      case GET_LOCATIONS_ERROR:
         return { ...state, places: null, error: action.error};
         break;

      case GET_ITINERARIES_START:
         return  { ...state};
         break;
      case GET_ITINERARIES_SUCCESS:
         return {...state};
         break;
      case GET_ITINERARIES_ERROR:
         return { ...state};
         break;

      case GET_LOGIN_START:
         return  { ...state};
         break;
      case GET_LOGIN_SUCCESS:
         return {...state, token: action.results.data};
         break;
      case GET_LOGIN_ERROR:
         return { ...state, token: null, error: action.error};
         break;

      default:
         return {...state};
   }
}