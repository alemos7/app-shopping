import {GET_LOGIN_START} from "../../consts/actionTypes";

export const getToken = payload => ({
   type: GET_LOGIN_START,
   payload
});

